<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">  
</head>
<body>
    <div class="container mt-2">
        
        <div class="row">
            <div class="col-md-8 mt-1 mb-1">
                <h2>Insert update invoice</h2>

            </div>
            <div class="container mt-5" style="max-width: 555px;">
                <div class="card-header alert alert-warning text-center mb-3">
                    <h2>Search</h2>
                </div>
                <input type="text" class="form-control" name="live_search" id="live_search" autocomplete="off" placeholder="Search...">
                <div id="search_result"></div>
            </div>
            <div class="col-md-8 nt-1 mb-2">
                <button type="button" id="addNewInv" class="btn btn-success">
                    Add
                </button>
            </div>
            <div class="col-md-8">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">quantity</th>
                            <th scope="col">description</th>
                            <th scope="col">unit price</th>
                            <th scope="col">date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            include 'db.php';
                            $query="select * from invoice limit 150";
                            $result = mysqli_query($conn, $query);
                        ?>
                        <?php if ($result->num_rows > 0): ?>
                        <?php while($arry=mysqli_fetch_row($result)): ?>
                        <tr>
                            <th><?php echo $arry[0]; ?></th>
                            <td><?php echo $arry[1]; ?></td>
                            <td><?php echo $arry[2]; ?></td>
                            <td><?php echo $arry[3]; ?></td>
                            <td><?php echo $arry[4]; ?></td>

                            <td>
                                <a href="javascript:void(0)" class="btn btn-primary edit" data-id="<?php echo $arry[0]; ?>">Edit</a>
                                <a href="javascript:void(0)" class="btn btn-primary delete" data-id="<?php echo $arry[0]; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php endwhile; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3" rowspan="1" headers="">Not found</td>
                            </tr>
                        <?php endif; ?>
                        <?php mysqli_free_result($result); ?>
                    </tbody>
                </table>

            </div>

        </div>

    </div>

    <div class="modal fade" id="inv-model" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="invModel"></h4>
                </div>
                <div class="modal-body">
                    <form action="javascript:void(0)" id="invInsertUpdateForm" class="form-horizontal" method="POST">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">description</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="description" name="description" placeholder="description" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="unitprice" class="col-sm-2 control-label">unit price</label>
                            <div class="col-sm-12">
                                <input step="any" type="number" class="form-control" id="unitprice" name="unitprice" placeholder="unitprice" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date" class="col-sm-2 control-label">date</label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control" id="date" name="date" placeholder="date" value="" required>
                            </div>
                        </div>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="btn-save" value="addNewInv">
                                Save changes
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>

        </div>

    </div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {
       $('#addNewInv').click(function() {
        $('#invInsertUpdateForm').trigger('reset');
        $('#invModel').html("Add New Invoice");
        $('#inv-model').modal('show');

           
       });

       $('body').on('click', '.edit', function() {
           var id = $(this).data('id');

           $.ajax({
               type: 'POST',
               url: 'edit.php',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function (res) {
                   $('#invModel').html('Edit Invoice');
                   $('#inv-model').modal('show');
                   $('#id').val(res.id);
                   $('#quantity').val(res.quantity);
                   $('#description').val(res.description);
                   $('#unitprice').val(res.unitprice);
                   $('#date').val(res.date);

                   
                   
               }
               

           });
           
           
       });

       

    

       $('body').on('click', '.delete', function() {
           if (confirm('Delete record?') == true) {
               var id = $(this).data('id');

               $.ajax({
                   type: 'POST',
                   url: 'delete.php',
                   data: {
                       id: id
                   },
                   dataType: 'json',
                   success: function (res) {
                       $('#quantity').html(res.quantity);
                       $('#description').html(res.description);
                       $('#unitprice').html(res.unitprice);
                       $('#date').html(res.date);
                       window.location.reload();
                       
                   }

               });
           }
           
       });
       $('#invInsertUpdateForm').submit(function() {
           $.ajax({
               type: 'POST',
               url: 'insert-update.php',
               data: $(this).serialize(),
               dataType: 'json',
               success: function (res) {
                   window.location.reload();
                   
               }

           });
           
       });


       
        
    });

    $(document).ready(function () {
        $('#live_search').keyup(function () {
            var query = $(this).val();
            if (query !="") {
                $.ajax({
                    url: 'ajax.php',
                    method: 'POST',
                    data: {
                        query: query
                    },
                    success: function (data) {
                        $('#search_result').html(data);
                        $('#search_result').css('display', 'block');

                        $('#live_search').focusout(function () {
                            $('#search_result').css('display', 'none');
                            
                        });
                        $('#live_search').focusin(function() {
                            $('#search_result').css('display', 'block');
                            
                        });
                        
                    }

                });

                
            }else{
                $('#search_result').css('display', 'none');
            }
            
        });
        
    });

    

</script>
</body>
</html>